FROM registry.i.indevsoftware.io/indev/base/python:1.0.0 AS base

# install runtime dependencies, including plugin dependencies
RUN --mount=type=cache,id=apt,target=/var/cache/apt <<EOS
	apt-get update
	apt install -y --no-install-recommends \
		borgbackup \
		default-mysql-client \
		docker.io \
		postgresql-client
	rm -rf /var/lib/apt/lists/*
EOS

FROM base AS builder

# install build dependencies
RUN --mount=type=cache,id=apt,target=/var/cache/apt <<EOS
	apt-get update
	apt install -y --no-install-recommends \
		docker-compose  # required for running integration tests
	rm -rf /var/lib/apt/lists/*
EOS

# install scripts
COPY scripts /app/scripts
ENV PATH=/app/scripts:$PATH

# install python dependencies
WORKDIR /app/stack/
COPY stack/pyproject.toml stack/poetry.lock .
RUN \
	--mount=type=cache,id=poetry,target=/app/cache/poetry \
	poetry install --no-interaction --no-root

# add stack
COPY stack .
RUN poetry install --no-interaction --only-root
