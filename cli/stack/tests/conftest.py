import os
import tempfile

import pytest

from queen.parsing import config


@pytest.fixture()
def make_toml(tmp_path):
    paths_to_delete = []

    def _make_toml(contents, *, base_dir=None):
        if base_dir is None:
            base_dir = tmp_path

        fd, path = tempfile.mkstemp(dir=base_dir, suffix=".toml", text=False)
        os.write(fd, contents.encode("utf-8"))
        os.close(fd)
        paths_to_delete.append(path)
        return path

    yield _make_toml

    for path in paths_to_delete:
        os.remove(path)


@pytest.fixture()
def patch_config_path(monkeypatch):
    def _patch_config_path(path):
        monkeypatch.setattr(config, "CONFIG_PATH", path)

    return _patch_config_path


@pytest.fixture()
def make_config(patch_config_path, make_toml):
    def _make_config(contents):
        path = make_toml(contents)
        patch_config_path(path)

    return _make_config


@pytest.fixture(scope="session")
def docker_compose_command() -> str:
    return "docker-compose"


@pytest.fixture(scope="session")
def docker_ip() -> str:
    return "host.docker.internal"
