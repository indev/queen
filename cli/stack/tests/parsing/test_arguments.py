import sys

import pytest

from queen import parsing
from queen.parsing.arguments import _discover_sites


def test_site_config():
    site_config_path = "/path/to/site/config.toml"

    args = parsing.parse_arguments(["-s", site_config_path])

    site_configs = args["site_configs"]
    assert len(site_configs) == 1
    assert site_configs[0] == site_config_path


@pytest.mark.parametrize("is_interactive", [True, False])
def test_interactive(monkeypatch, is_interactive):
    monkeypatch.setattr(sys.stdout, "isatty", lambda: is_interactive)

    args = parsing.parse_arguments([])

    assert args["interactive"] is is_interactive


def test_site_discovery(tmp_path, make_toml):
    make_toml("")
    make_toml("")

    sites = _discover_sites(tmp_path)

    assert len(list(sites)) == 2
