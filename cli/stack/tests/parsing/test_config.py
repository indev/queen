import os.path

import pytest

from queen import parsing
from queen.exceptions import QueenException


def test_missing_config(patch_config_path):
    patch_config_path("/should/not/exist.toml")

    with pytest.raises(QueenException, match="No such file or directory"):
        parsing.parse_config()


def test_full_config(make_config):
    make_config(
        """
        repo_prefix = 'some@repo:prefix'
        sentry_dsn = 'some sentry dsn'

        [logging]
        directory = '/some/directory/'

        [borg_env]
        FOO = 'bar'

        [plugins.postgres]
        foo = 'bar'
        """
    )
    config = parsing.parse_config()

    assert config.repo_prefix == "some@repo:prefix"
    assert config.sentry_dsn == "some sentry dsn"
    assert config.log_directory == "/some/directory/"
    assert config.borg_env["FOO"] == "bar"
    assert config.plugins["postgres"]["foo"] == "bar"


def test_missing_repo_prefix(make_config):
    make_config(
        """
        sentry_dsn = 'some sentry dsn'

        [logging]
        directory = '/some/directory/'
        """
    )

    with pytest.raises(QueenException, match="repo_prefix.*required"):
        parsing.parse_config()


def test_absolute_local_repo_prefix(make_config):
    prefix = "/absolute/prefix"
    make_config(
        f"""
        repo_prefix = '{prefix}'
        sentry_dsn = 'some sentry dsn'

        [logging]
        directory = '/some/directory/'
        """
    )

    config = parsing.parse_config()
    assert config.repo_prefix == prefix


def test_relative_local_repo_prefix(make_config):
    relative_prefix = "relative/prefix"

    make_config(
        f"""
        repo_prefix = '{relative_prefix}'
        sentry_dsn = 'some sentry dsn'

        [logging]
        directory = '/some/directory/'
        """
    )

    config = parsing.parse_config()
    assert config.repo_prefix == os.path.abspath(relative_prefix)


def test_missing_sentry_dsn(make_config):
    make_config(
        """
        repo_prefix = 'some@repo:prefix'

        [logging]
        directory = '/some/directory/'
        """
    )

    with pytest.raises(QueenException, match="sentry_dsn.*required"):
        parsing.parse_config()


def test_missing_logging_directory(make_config):
    make_config(
        """
        repo_prefix = 'some@repo:prefix'
        sentry_dsn = 'some sentry dsn'
        """
    )

    with pytest.raises(QueenException, match="logging.directory.*required"):
        parsing.parse_config()


def test_relative_logging_directory(make_config):
    make_config(
        """
        repo_prefix = 'some@repo:prefix'
        sentry_dsn = 'some sentry dsn'

        [logging]
        directory = 'relative/directory/'
        """
    )
    with pytest.raises(QueenException, match="logging.directory.*absolute"):
        parsing.parse_config()


def test_borg_env_not_a_table(make_config):
    make_config(
        """
        repo_prefix = 'some@repo:prefix'
        sentry_dsn = 'some sentry dsn'
        plugins = 'bar'

        borg_env = 'invalid'

        [logging]
        directory = '/some/directory/'
        """
    )

    with pytest.raises(QueenException, match="borg_env.*table"):
        parsing.parse_config()


def test_plugins_not_a_table(make_config):
    make_config(
        """
        repo_prefix = 'some@repo:prefix'
        sentry_dsn = 'some sentry dsn'
        plugins = 'bar'

        [logging]
        directory = '/some/directory/'

        """
    )

    with pytest.raises(QueenException, match="plugins.*table"):
        parsing.parse_config()


def test_plugins_plugin_not_a_table(make_config):
    make_config(
        """
        repo_prefix = 'some@repo:prefix'
        sentry_dsn = 'some sentry dsn'

        [logging]
        directory = '/some/directory/'

        [plugins]
        foo = 'bar'
        """
    )

    with pytest.raises(QueenException, match="plugins.foo.*table"):
        parsing.parse_config()
