import pytest

from queen import exceptions, parsing


def test_correct_config(make_toml):
    path = make_toml(
        """
        project_name = 'test project'
        passphrase = 'test secret'
        paths = [
            '/test/path'
        ]
        """
    )
    config = parsing.parse_site(path)

    assert config["project_name"] == "test project"
    assert config["passphrase"] == "test secret"
    assert "/test/path" in config["paths"]


def test_missing_passphrase(make_toml):
    path = make_toml(
        """
        project_name = 'test project'
        paths = [
            '/test/path'
        ]
        """
    )

    with pytest.raises(exceptions.QueenException):
        parsing.parse_site(path)


def test_missing_project_name(make_toml):
    path = make_toml(
        """
        passphrase = 'test secret'
        paths = [
            '/test/path'
        ]
        """
    )

    with pytest.raises(exceptions.QueenException):
        parsing.parse_site(path)
