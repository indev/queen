import subprocess

import pytest

from queen.exceptions import QueenException
from queen.plugins import docker_volume


@pytest.fixture
def docker_volume_setup(docker_compose_project_name, docker_services):
    volume = f"{docker_compose_project_name}_test_volume"

    docker_run_args = ["docker", "run", "--rm", "-v", f"{volume}:/mnt", "alpine"]
    create_args = ["/bin/sh", "-c", 'printf "hello\n" > /mnt/test']

    subprocess.run(
        docker_run_args + create_args,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )

    yield volume

    delete_args = ["rm", "/mnt/test"]
    subprocess.run(docker_run_args + delete_args)


def test_missing_volume_name():
    with pytest.raises(QueenException):
        docker_volume.backup({}, [{}], "/does/not/exist")


@pytest.mark.docker
def test_docker_volume_integration(tmp_path, docker_volume_setup):
    volume_name = docker_volume_setup

    config_dict = {"name": volume_name}

    docker_volume.backup({}, [config_dict], tmp_path)

    subprocess.run(
        ["tar", "xf", f"{volume_name}.tar"],
        cwd=tmp_path,
    )

    with open(tmp_path / "test") as f:
        contents = f.read()

    assert contents == "hello\n"
