from queen import plugins


def test_plugin_backup(mocker):
    mocker.patch("os.mkdir")
    patched_postgres_backup = mocker.Mock()
    patched_mysql_backup = mocker.Mock()

    mocker.patch.dict(
        plugins.plugin_map,
        {
            "postgres": patched_postgres_backup,
            "mysql": patched_mysql_backup,
        },
    )

    global_config = {}
    plugin_configs = {
        "postgres": [{}, {}],
        "mysql": [{}],
    }

    plugins.backup(global_config, plugin_configs, "project_name", "/some/path")

    assert patched_postgres_backup.call_count == 1
    assert patched_mysql_backup.call_count == 1
