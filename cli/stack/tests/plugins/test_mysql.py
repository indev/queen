import subprocess

import pytest

from queen.plugins import mysql


def is_mysql_up(host, port, user, password):
    args = [
        "mysql",
        f"--host={host}",
        f"--port={port}",
        f"--user={user}",
        f"--password={password}",
    ]
    p = subprocess.run(args, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

    return p.returncode == 0


@pytest.fixture(scope="session")
def mysql_service(docker_ip, docker_services):
    user = "root"
    password = "password"
    host = docker_ip
    port = docker_services.port_for("mysql", 3306)

    params = {
        "host": host,
        "port": port,
        "user": user,
        "password": password,
    }

    docker_services.wait_until_responsive(
        timeout=30.0,
        pause=0.5,
        check=lambda: is_mysql_up(**params),
    )

    return params


@pytest.fixture
def mysql_setup(mysql_service):
    database = "test_database"
    table = "test_table"
    args = [
        "mysql",
        f"--host={mysql_service['host']}",
        f"--port={mysql_service['port']}",
        f"--user={mysql_service['user']}",
        f"--password={mysql_service['password']}",
    ]

    create_p = subprocess.Popen(args, stdin=subprocess.PIPE)
    create_sql = f"""
        CREATE DATABASE {database};
        use {database};
        CREATE TABLE {table} (test_column int);
    """
    create_p.communicate(create_sql.encode("utf-8"))

    yield {
        "database": database,
        "table": table,
        "user": mysql_service["user"],
        "password": mysql_service["password"],
        "host": mysql_service["host"],
        "port": mysql_service["port"],
    }

    drop_p = subprocess.Popen(args, stdin=subprocess.PIPE)
    drop_sql = rf"DROP DATABASE {database}"
    drop_p.communicate(drop_sql.encode("utf-8"))


@pytest.mark.docker
@pytest.mark.mysql
def test_mysql_backup(tmp_path, mysql_setup):
    config_dict = {
        "database": mysql_setup["database"],
        "user": mysql_setup["user"],
        "password": mysql_setup["password"],
        "host": mysql_setup["host"],
        "port": mysql_setup["port"],
    }

    mysql.backup({}, [config_dict], tmp_path)

    dump_path = tmp_path / f'{mysql_setup["database"]}.dump.sql'
    with dump_path.open(encoding="utf-8") as f:
        dump_contents = f.read()

    assert mysql_setup["database"] in dump_contents
    assert mysql_setup["table"] in dump_contents
