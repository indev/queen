import pytest

from queen.exceptions import QueenException
from queen.plugins import postgres


def test_make_connection_string_full():
    cs = postgres._make_connection_string(
        "database",
        user="user",
        password="password",
        host="host",
        port=123,
    )

    assert cs == "postgresql:///database?user=user&password=password&host=host&port=123"


def test_make_connection_string_partial():
    cs = postgres._make_connection_string(
        "database",
        user="user",
        password=None,
        host="host",
        port=None,
    )

    assert cs == "postgresql:///database?user=user&host=host"


def test_make_connection_string_with_only_database():
    cs = postgres._make_connection_string(
        "database",
        user=None,
        password=None,
        host=None,
        port=None,
    )

    assert cs == "postgresql:///database"


def test_make_connection_string_with_spaces():
    cs = postgres._make_connection_string(
        "data base",
        user="user",
        password="pass word",
        host="host",
        port=123,
    )

    assert cs == "postgresql:///data%20base?user=user&password=pass%20word&host=host&port=123"


def test_parse_config_full():
    config = postgres._parse_config(
        {},
        {
            "database": "database",
            "user": "user",
            "password": "password",
            "host": "host",
            "port": 123,
            "pg_dump": ["sudo", "-iu", "postgres"],
        },
    )

    assert config.database == "database"
    assert config.user == "user"
    assert config.password == "password"
    assert config.host == "host"
    assert config.port == 123
    assert config.pg_dump == ["sudo", "-iu", "postgres"]


def test_parse_config_partial():
    config = postgres._parse_config(
        {},
        {
            "database": "database",
            "user": "user",
            "host": "host",
        },
    )

    assert config.database == "database"
    assert config.user == "user"
    assert config.host == "host"
    assert config.password is None
    assert config.port is None
    assert config.pg_dump == ["pg_dump"]


def test_parse_config_missing_database():
    with pytest.raises(QueenException):
        postgres._parse_config(
            {},
            {
                "user": "user",
                "password": "password",
                "host": "host",
                "port": 123,
            },
        )


def test_parse_config_invalid_pg_dump():
    with pytest.raises(QueenException, match="pg_dump.*list"):
        postgres._parse_config(
            {},
            {"database": "database", "pg_dump": "test"},
        )


def test_parse_config_extra_fields():
    with pytest.raises(QueenException, match="unsupported.*foo"):
        postgres._parse_config(
            {},
            {
                "database": "database",
                "foo": "bar",
            },
        )


def test_parse_config_merging():
    config = postgres._parse_config(
        {"user": "user", "host": "host"},
        {"database": "database", "host": "otherhost"},
    )

    assert config.database == "database"
    assert config.user == "user"
    assert config.host == "otherhost"
    assert config.password is None
    assert config.port is None


def test_parse_config_merging_missing_database():
    with pytest.raises(QueenException):
        postgres._parse_config(
            {"user": "user", "host": "host"},
            {"host": "otherhost"},
        )
