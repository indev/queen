import subprocess

import pytest

from queen.plugins import postgres


def is_postgres_up(connection_string):
    p = subprocess.run(
        ["pg_isready", "-d", connection_string],
        stdin=subprocess.DEVNULL,
        stdout=subprocess.DEVNULL,
    )

    return p.returncode == 0


@pytest.fixture(scope="session")
def postgres_service(docker_ip, docker_services):
    user = "postgres"
    password = "password"
    host = docker_ip
    port = docker_services.port_for("postgres", 5432)

    connection_string = f"postgresql://{user}:{password}@{host}:{port}"

    docker_services.wait_until_responsive(
        timeout=30.0,
        pause=0.5,
        check=lambda: is_postgres_up(connection_string),
    )

    return {
        "user": user,
        "password": password,
        "host": host,
        "port": port,
        "connection_string": connection_string,
    }


@pytest.fixture
def postgres_setup(postgres_service):
    database = "test_database"
    table = "test_table"

    create_p = subprocess.Popen(
        ["psql", postgres_service["connection_string"]],
        stdin=subprocess.PIPE,
    )
    create_sql = rf"""
        CREATE DATABASE {database};
        \c {database};
        CREATE TABLE {table} ();
    """
    create_p.communicate(create_sql.encode("utf-8"))

    yield {
        "database": database,
        "table": table,
        "user": postgres_service["user"],
        "password": postgres_service["password"],
        "host": postgres_service["host"],
        "port": postgres_service["port"],
    }

    drop_p = subprocess.Popen(
        ["psql", postgres_service["connection_string"]],
        stdin=subprocess.PIPE,
    )
    drop_sql = rf"DROP DATABASE {database}"
    drop_p.communicate(drop_sql.encode("utf-8"))


@pytest.mark.docker
@pytest.mark.postgres
def test_postges_backup(tmp_path, postgres_setup):
    config_dict = {
        "database": postgres_setup["database"],
        "user": postgres_setup["user"],
        "password": postgres_setup["password"],
        "host": postgres_setup["host"],
        "port": postgres_setup["port"],
    }

    postgres.backup({}, [config_dict], tmp_path)

    dump_name = tmp_path / f'{postgres_setup["database"]}.pgdump'
    p = subprocess.run(
        ["pg_restore", "-l", dump_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    stdout = p.stdout.decode("utf-8")
    assert postgres_setup["database"] in stdout
    assert postgres_setup["table"] in stdout
