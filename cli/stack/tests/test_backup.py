import json
import logging
import os.path
import shutil
import subprocess

import pytest

from queen import backup, exceptions, plugins, processes
from queen.parsing.config import Config


def _create_data_file(data_path, contents):
    file_path = data_path / "file"

    with open(file_path, "w") as f:
        f.write(contents)

    return file_path


def _get_contents_from_repo(monkeypatch, borg_repo_path, file_path):
    monkeypatch.setenv("BORG_PASSPHRASE", "passphrase")

    # get name of created archive
    list_p = subprocess.Popen(
        ["borg", "list", "--last", "1", "--json", borg_repo_path],
        stdout=subprocess.PIPE,
    )
    list_out, _ = list_p.communicate()
    parsed_list = json.loads(list_out)
    archive_name = parsed_list["archives"][0]["archive"]

    extract_p = subprocess.Popen(
        [
            "borg",
            "extract",
            "--stdout",
            f"{borg_repo_path}::{archive_name}",
            str(file_path)[1:],  # strip leading slash
        ],
        stdout=subprocess.PIPE,
        text=True,
    )
    extract_out, _ = extract_p.communicate()

    return extract_out


@pytest.fixture
def borg_repo_path(tmp_path):
    repo_path = tmp_path / "fixture-repo"
    passphrase = "passphrase"

    backup._ensure_repo(repo_path, passphrase)

    yield repo_path

    shutil.rmtree(repo_path)


def test_environ_with_pass(mocker):
    mocker.patch.dict("os.environ", {"FOO": "BAR"}, clear=True)

    new_environ = backup._environ_with_pass("passphrase")

    assert new_environ["FOO"] == "BAR"
    assert new_environ["BORG_PASSPHRASE"] == "passphrase"


@pytest.mark.borg
def test_ensure_repo(tmp_path, monkeypatch):
    repo_path = tmp_path / "repo"
    passphrase = "passphrase"

    backup._ensure_repo(repo_path, passphrase)

    monkeypatch.setenv("BORG_PASSPHRASE", passphrase)
    p = subprocess.Popen(["borg", "info", repo_path])
    code = p.wait()
    assert code == 0


@pytest.mark.borg
def test_ensure_repo_on_existing(tmp_path):
    repo_path = tmp_path / "repo"

    backup._ensure_repo(repo_path, "passphrase")
    backup._ensure_repo(repo_path, "passphrase")


@pytest.mark.borg
def test_ensure_repo_other_failure(tmp_path):
    # use a path that doesn't exist to force an unrelated borg error
    repo_path = tmp_path / "doesnot" / "exist"

    with pytest.raises(exceptions.QueenSubprocessException):
        backup._ensure_repo(str(repo_path), "passphrase")


@pytest.mark.borg
def test_create_archive(tmp_path, monkeypatch, borg_repo_path):
    contents = "hello\n"

    data_path = tmp_path / "data"
    data_path.mkdir()

    file_path = _create_data_file(data_path, contents)

    backup._create_archive(
        borg_repo_path,
        "passphrase",
        {},
        [data_path],
        temp_dir=None,
        interactive=False,
    )

    restored = _get_contents_from_repo(monkeypatch, borg_repo_path, file_path)
    assert restored == contents


@pytest.mark.borg
def test_create_archive_with_temp_dir(tmp_path, monkeypatch, borg_repo_path):
    contents = "hello\n"

    data_path = tmp_path / "data"
    data_path.mkdir()

    file_path = _create_data_file(data_path, contents)

    backup._create_archive(
        borg_repo_path,
        "passphrase",
        {},
        [data_path],
        temp_dir=str(tmp_path),
        interactive=False,
    )

    restored = _get_contents_from_repo(monkeypatch, borg_repo_path, file_path)
    assert restored == contents


@pytest.mark.borg
def test_create_archive_interactive(tmp_path, monkeypatch, capsys, borg_repo_path):
    contents = "hello\n"

    data_path = tmp_path / "data"
    data_path.mkdir()

    file_path = _create_data_file(data_path, contents)

    backup._create_archive(
        borg_repo_path,
        "passphrase",
        {},
        [data_path],
        temp_dir=None,
        interactive=True,
    )

    out, _ = capsys.readouterr()

    assert len(out) > 0

    restored = _get_contents_from_repo(monkeypatch, borg_repo_path, file_path)
    assert restored == contents


def test_site(tmp_path, mocker):
    config = Config(
        repo_prefix="/some/prefix/",
        sentry_dsn="",
        log_stdout=True,
        log_directory="",
        borg_env={},
        plugins={},
    )

    project_name = "project"
    passphrase = "passphrase"
    path = str(tmp_path)
    site_config = {
        "project_name": project_name,
        "passphrase": passphrase,
        "paths": [path],
    }

    mocker.patch("queen.backup._ensure_repo")
    mocker.patch("queen.backup._create_archive")

    backup.site(config, site_config, interactive=False)

    repo = os.path.join(config.repo_prefix, project_name)

    backup._ensure_repo.assert_called_once_with(repo, passphrase)
    backup._create_archive.assert_called_once_with(repo, passphrase, {}, [path], None, False)


def test_site_with_missing_path(caplog, tmp_path, mocker):
    config = Config(
        repo_prefix="/some/prefix/",
        sentry_dsn="",
        log_stdout=True,
        log_directory="",
        borg_env={},
        plugins={},
    )

    project_name = "project"
    passphrase = "passphrase"
    path = str(tmp_path)
    missing_path = "/should/not/exist/"
    site_config = {
        "project_name": project_name,
        "passphrase": passphrase,
        "paths": [path, missing_path],
    }

    caplog.set_level(logging.WARNING)
    mocker.patch("queen.backup._ensure_repo")
    mocker.patch("queen.backup._create_archive")

    backup.site(config, site_config, interactive=False)

    repo = os.path.join(config.repo_prefix, project_name)

    backup._ensure_repo.assert_called_once_with(repo, passphrase)
    backup._create_archive.assert_called_once_with(repo, passphrase, {}, [path], None, False)

    # check for appropriate logs
    assert len(caplog.records) == 1
    record = caplog.records[0]
    assert record.levelno == logging.WARNING
    assert "skipping missing path" in record.message
    assert missing_path in record.message


def test_site_with_borg_env(tmp_path, mocker):
    config = Config(
        repo_prefix="/some/prefix/",
        sentry_dsn="",
        log_stdout=True,
        log_directory="",
        borg_env={"FOO": "BAR"},
        plugins={},
    )
    site_config = {
        "project_name": "project_name",
        "passphrase": "passphrase",
        "paths": ["/some/path"],
    }

    mocker.patch("queen.backup._ensure_repo")
    mocker.patch("queen.processes.run")

    backup.site(config, site_config, interactive=False)

    _, call_kwargs = processes.run.call_args
    assert call_kwargs["env"]["FOO"] == "BAR"


def test_site_with_plugins(tmp_path, mocker):
    config = Config(
        repo_prefix="/some/prefix/",
        sentry_dsn="",
        log_stdout=True,
        log_directory="",
        borg_env={},
        plugins={"postgres": {"a": 1}},
    )

    project_name = "project"
    passphrase = "passphrase"
    path = str(tmp_path)
    site_config = {
        "project_name": project_name,
        "passphrase": passphrase,
        "paths": [path],
        "plugins": {"postgres": {"b": 2}},
    }

    mocker.patch("queen.backup._ensure_repo")
    mocker.patch("queen.plugins.backup")
    mocker.patch("queen.backup._create_archive")

    backup.site(config, site_config, interactive=False)

    repo = os.path.join(config.repo_prefix, project_name)

    backup._ensure_repo.assert_called_once_with(repo, passphrase)

    plugins.backup.assert_called_once()
    plugin_args, _ = plugins.backup.call_args
    assert plugin_args[0] == {"postgres": {"a": 1}}
    assert plugin_args[1] == {"postgres": {"b": 2}}
    assert plugin_args[2] == project_name

    backup._create_archive.assert_called_once()
    create_archive_args, _ = backup._create_archive.call_args
    assert create_archive_args[0] == repo
    assert create_archive_args[1] == passphrase
    assert create_archive_args[2] == {}
    assert create_archive_args[3] == [path, "_queen"]
