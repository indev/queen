import logging
import os

from queen import logs


def test_log_setup(tmp_path):
    logs.config(str(tmp_path), False)

    logger = logging.getLogger("queen")

    for handler in logger.handlers:
        assert handler.baseFilename.startswith(str(tmp_path))

    # remove the handlers (and their files) that logs.config just added to avoid actually logging
    # on pytest's tmp_path
    while logger.handlers:
        # can't use for loop since removeHandler modifies logger.handlers
        handler = logger.handlers[0]

        logger.removeHandler(handler)
        os.remove(handler.baseFilename)
