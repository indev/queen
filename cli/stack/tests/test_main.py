import logging

import pytest
import sentry_sdk

from queen import backup, logs
from queen.exceptions import QueenException
from queen.main import main
from queen.parsing import arguments


@pytest.fixture
def site_config_path(tmp_path, monkeypatch):
    path = tmp_path / "sites.d"
    path.mkdir()

    monkeypatch.setattr(arguments, "SITE_CONFIG_DIR", path)
    return path


@pytest.fixture
def make_site_config(site_config_path, make_toml):
    def _make_site_config(contents):
        make_toml(contents, base_dir=site_config_path)

    return _make_site_config


@pytest.fixture
def setup_main(mocker, make_config):
    mocker.patch("sys.argv", ["queen"])
    mocker.patch("sentry_sdk.init")
    mocker.patch("queen.logs.config")
    mocker.patch("queen.backup.site")

    make_config(
        """
        repo_prefix = 'some@repo:prefix'
        sentry_dsn = 'some sentry dsn'

        [logging]
        directory = '/some/directory/'
        """
    )


def test_main(setup_main):
    main()

    sentry_sdk.init.assert_called_once_with("some sentry dsn")
    logs.config.assert_called_once_with("/some/directory/", False)


def test_main_with_site_configs(mocker, setup_main, make_site_config):
    make_site_config(
        """
        project_name = 'project 1'
        passphrase = 'secret 1'
        paths = [
            '/test/path1'
        ]
        """
    )
    make_site_config(
        """
        project_name = 'project 2'
        passphrase = 'secret 2'
        paths = [
            '/test/path2'
        ]
        """
    )

    main()

    assert backup.site.call_count == 2


def test_main_with_broken_site_config(caplog, mocker, setup_main, make_site_config):
    make_site_config("")
    caplog.set_level(logging.ERROR)
    mocker.patch("sentry_sdk.capture_exception")

    main()

    assert len(caplog.records) == 1
    assert sentry_sdk.capture_exception.call_count == 1


def test_main_with_failed_backup(caplog, mocker, setup_main, make_site_config):
    make_site_config(
        """
        project_name = 'project 1'
        passphrase = 'secret 1'
        paths = [
            '/test/path1'
        ]
        """
    )
    caplog.set_level(logging.ERROR)
    mocker.patch("sentry_sdk.capture_exception")

    def failed_backup(*args, **kwargs):
        raise QueenException()

    mocker.patch("queen.backup.site", failed_backup)

    main()

    assert len(caplog.records) == 1
    assert sentry_sdk.capture_exception.call_count == 1
