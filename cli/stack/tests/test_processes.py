import pytest

from queen import exceptions, processes


def test_succesful_launch():
    code, stderr = processes.launch(["true"])

    assert code == 0


def test_failed_launch():
    code, stderr = processes.launch(["false"])

    assert code != 0


def test_successful_run():
    processes.run(["true"])


def test_failed_run():
    with pytest.raises(exceptions.QueenSubprocessException) as e:
        processes.run(["false"])

    assert e.value.code != 0


def test_failed_run_with_stderr():
    output = "test"

    with pytest.raises(exceptions.QueenSubprocessException) as e:
        processes.run(["/bin/sh", "-c", f"printf '{output}' >&2; exit 1"])

    assert e.value.code != 0
    assert e.value.stderr == output


def test_run_interactive(capsys):
    processes.run_interactive(["/bin/sh", "-c", r"printf line1\\nline2\\n >&2"])

    out, _ = capsys.readouterr()
    assert out == "line1\rline2\r"


def test_failed_run_interactive():
    with pytest.raises(exceptions.QueenSubprocessException) as e:
        processes.run_interactive(["false"])

    assert e.value.code != 0
